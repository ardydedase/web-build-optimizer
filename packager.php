<?php
/**
 * packager.php
 * 
 * parameters: 
 * 
 * -j java path 
 * -b build location/path
 * -v version number: if not specified, script will auto-generate based on the existing versions
 * 
 * e.g. php packager.php -j /opt/java1.6/ -b /var/www/ -v 1.1
 * 
 */

/**
 * Iterate through all the files in a directory
 * This will search for template files (*.tpl.html)
 */
class TemplateFilterIterator extends FilterIterator
{
    private $file_pattern = "/\.tpl.html/i";
    
    public function __construct(Iterator $iter)
    {
        parent::__construct($iter);
    }
    
    public function accept()
    {
        $file = $this->getInnerIterator()->current();
        
        return preg_match($this->file_pattern, $file->getFilename());
    }

}

/**
 * Iterate through all the folders in a directory
 * This will search for folders.
 */
class FolderFilterIterator extends FilterIterator
{
    public function __construct(Iterator $iter)
    {
        parent::__construct($iter);
    }
    
    public function accept()
    {
        $file = $this->getInnerIterator()->current();
        
        return is_dir($file->getPathName());       
    }
}


class FileFilterIterator extends FilterIterator
{
    public function __construct(Iterator $iter)
    {
        parent::__construct($iter);
    }
    
    public function accept()
    {
        $file = $this->getInnerIterator()->current();
        
        // make sure that it's not a folder coz S3 will automatically create that for you
        return !is_dir($file->getPathName()) && file_exists($file->getPathName());       
    }
}

include('lib/config.inc.php');
include('lib/config.class.php');
include('lib/templateoptimizer.class.php');
include('lib/compressor.class.php');
include('lib/sdk-1.5.5/sdk.class.php');

/**
 * Start the ugly procedural script
 * 
 * 
 */

// get all the valid arguments data
Config::capture_args($argv);
// set config variables
Config::set_variables();

// create the versioned resources path

//echo "versioned path: {Config::$versioned_htdocs_path}";
printf("versioned path: %s \n", Config::$versioned_htdocs_path);

if (!is_dir(Config::$versioned_htdocs_path)) 
{   
    if (mkdir(Config::$versioned_htdocs_path))
    {
        // successfully created the version folder
        printf("created %s \n", Config::$versioned_htdocs_path);
    }
    else 
    {
        // something went wrong
        exit('unable to create the folder: ' . Config::$versioned_htdocs_path);
    }
   
}

// run the shell script that uses rsync to copy resources files to the version folder
// format our command to run our shell script

$cmd = sprintf('sh %s %s %s %s', Config::$version_rsync, 'resources', Config::$htdocs_path . '/', Config::$versioned_htdocs_path . '/');

echo Config::$versioned_htdocs_path . '/';

$shell_out = shell_exec($cmd);    
echo "output: $shell_out\n";

// check if compressed folder exist, create if otherwise
if(!is_dir(Config::$compressed_resources_path))
{
    if(mkdir(Config::$compressed_resources_path))
    {
        // successfully created the compressed folder
        printf("created %s \n", Config::$compressed_resources_path);

        foreach(array('js', 'css', 'img') as $asset_folder)
        {
            if(mkdir(Config::$compressed_resources_path . '/' . $asset_folder))
            {
                // successfully created the compressed asset folder
                printf("created %s \n", Config::$compressed_resources_path . '/' . $asset_folder);                
            }
        }
    }
}

// make sure that all folders/subfolders are set in our optimized_templates folder
$template_it = new FolderFilterIterator(
    new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator(Config::$templates_path),
        RecursiveIteratorIterator::SELF_FIRST
    )
);

// create the optimized subfolders in case they don't exist
foreach ($template_it as $folder_path => $object) 
{
    if(TemplateOptimizer::prepare_deployment_folder($folder_path, Config::$optimized_templates_path, TemplateOptimizer::$_pattern_templates_path))
    {
        echo 'created the folder: ' . Config::$optimized_templates_path . PHP_EOL;
    }
    else
    {
        echo "couldn't create the folder: " . Config::$optimized_templates_path . PHP_EOL;   
    }
}

$it = new TemplateFilterIterator (
    new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator(Config::$templates_path),
        RecursiveIteratorIterator::SELF_FIRST
    )
);

echo 'iterating through the template files.' . PHP_EOL;
echo PHP_EOL;

// iterate throught each template file (*.tpl) in the directory
foreach ($it as $file_path => $object) 
{
    //if (Config::$templates_path . '/space_blue/3col_skeleton.tpl.html' == $file_path)
    //{
    echo "real path: $file_path" . PHP_EOL;
    TemplateOptimizer::optimize_template($file_path);        
    //}

}

// GZip compress the css and js files
$compressed_js_files = get_files(Config::$compressed_resources_path . '/js');
var_dump($compressed_js_files);

$compressed_css_files = get_files(Config::$compressed_resources_path . '/css');
var_dump($compressed_css_files);

$gzip = new GZipCompressor();
$gzip_res_js = $gzip->perform_compression($compressed_js_files);
$gzip_res_css = $gzip->perform_compression($compressed_css_files);

/**
 * This is the part where we upload all files to S3
 * 
 */

// class form Amazon AWS SDK
$s3 = new AmazonS3();
$s3->set_region(AmazonS3::REGION_APAC_SE1);
$s3->path_style = true;

// check if version folder already exists in S3
if (!$s3->if_object_exists(AWS_S3_BUCKET, Config::$package_version . '/'))
{
    // create new version's folder in s3
    $create_response = $s3->create_object(AWS_S3_BUCKET, Config::$package_version . '/', array(
        'contentType' => 'text/directory',
        'acl' => AmazonS3::ACL_PUBLIC,
        'storage' => AmazonS3::STORAGE_REDUCED
    ));
    
    if (200 === $create_response->status)
    {
        echo "\nSuccessfully created object folder: " . Config::$package_version;        
    }
    else
    {
        echo "\nProblem creating the object: " . Config::$package_version;
        var_dump($create_response);
        exit;
    }
}
else 
{
    echo "\nObject folder already exists in S3: " . Config::$package_version;
    
}

// make sure that all folders/subfolders are set in our resources folder
$s3_folders_it = new FolderFilterIterator(
    new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator(Config::$htdocs_path . '/' . Config::$package_version . '/resources/compressed'),
        RecursiveIteratorIterator::SELF_FIRST
    )
);

// need to create the directory structure first in S3 bucket before uploading the files
foreach ($s3_folders_it as $s3_folder_path => $object)
{
    
    $s3_relative_folder_path = strstr($s3_folder_path, '/resources/compressed');
    
    echo "\ncreating folder object: $s3_relative_folder_path";
    
    if (!$s3->if_object_exists(AWS_S3_BUCKET, Config::$package_version . "$s3_relative_folder_path/"))
    {
        // create the directory structure first
        $s3_create_dir_response = $s3->create_object(AWS_S3_BUCKET, Config::$package_version . "$s3_relative_folder_path/", array(
            'contentType' => 'text/directory',
            'acl' => AmazonS3::ACL_PUBLIC,
            'storage' => AmazonS3::STORAGE_REDUCED
        ));

        echo " status: {$s3_create_dir_response->status}\n";        
    }
    else
    {
        echo " skipping, folder already exists\n";
    }

}


// iterate through all the files in our compressed resources folder
$s3_files_it = new FileFilterIterator(
    new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator(Config::$htdocs_path . '/' . Config::$package_version . '/resources/compressed'),
        RecursiveIteratorIterator::SELF_FIRST
    )
);

//echo "S3 file path: \n";
foreach ($s3_files_it as $s3_upload_file_path => $object)
{
    $s3_relative_path = strstr($s3_upload_file_path, '/resources');        
    
    // check first if object already exists in the specified bucket    
    if ($s3->if_object_exists(AWS_S3_BUCKET, Config::$package_version . $s3_relative_path))    
    {   
        // If object already exists, delete it. So that we can delete it later on.        
        if ($s3->delete_object(AWS_S3_BUCKET, Config::$package_version . $s3_relative_path))
        {
            echo 'deleted the existing object: ' . Config::$package_version . $s3_relative_path;
        }
    }
    
    echo "\nuploading: $s3_relative_path to S3 ";
    
    // get extension
    $ext = pathinfo($s3_upload_file_path, PATHINFO_EXTENSION);    
    $content_encoding = ('js' === $ext || 'css' === $ext) ? 'gzip' : '';
    
    $headers = array(
        'Cache-Control' => 'max-age=15552000',
        'Content-Encoding' => $content_encoding
    );
    
    echo "headers for $s3_upload_file_path";
    var_dump($headers);
    echo "\n";
    
        
  
    $s3_upload_response = $s3->create_object(AWS_S3_BUCKET, Config::$package_version . $s3_relative_path, array(
        'fileUpload' => $s3_upload_file_path,
        'acl' => AmazonS3::ACL_PUBLIC,
        'storage' => AmazonS3::STORAGE_REDUCED,
        'headers' => $headers
    ));
    
    echo "status: $s3_upload_response->status\n";
}

// create/modify the build.php - a config file used by the corporate site
Config::modify_build_config();

// sync to the build directory when done.
Compressor::sync_to_build();


/**
 * Helper functions
 * 
 */

function get_files($path)
{

    $files_it = new FileFilterIterator(
        new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($path),
            RecursiveIteratorIterator::SELF_FIRST
        )
    );    
    
    foreach ($files_it as $file_path => $object)
    {
        $files[] = $file_path;
    }
    
    return $files;
}
/* End of file packager.php */
/* Location: ./build/deployment/packager.php*/