#!/bin/bash

# sync htdocs resources into the versioned folder 
# exclude everything except resources

rsync -avz --include=$1/ --include=$1/** --exclude=* $2 $3
