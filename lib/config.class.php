<?php
/**
 * 
 */

class Config
{    
    public static   $arguments, 
                    $options, 
                    $package_version, 
                    $build_path,
                    $source_path,
                    $source_application_path,
                    $templates_path,
                    $optimized_templates_path,
                    $htdocs_path,
                    $versioned_htdocs_path,
                    $resources_path,
                    $compressed_resources_path,
                    $scripts_path,
                    $compressed_url,
                    $compressed_url_string,
                    $css_theme_url,
                    $java,
                    $css_theme_path,
                    $config_path,
                    $build_config_file,
                    $cdn_url,
                    $cdn_compressed_url,
                    $cc_compilation_level,
                    $hc_compilation_level,
                    $closure_compiler,
                    $yui_compressor,
                    $html_compressor,
                    $version_rsync,
                    $build_rsync;
    
    public static function capture_args($argv)
    {   
        // we only recognize the following arguments: -s -v -j -b --use-cdn
        // -s source path
        // -v version
        // -j java path
        // -b target build path

        $shortopts = "s:v:j:b:";
        $longopts = array(
            "use-cdn",
            "advanced-optimizations"
        );
        
        $arguments = getopt($shortopts, $longopts);
        

        foreach ($argv as $option => $value)
        {    
            $longopt = '';
            $longopt = str_replace('--', '', $value);

            if ($longopt && in_array($longopt, $longopts))
            {
                $options[$longopt] = true;
                
                self::$options = $options;
            }
        }
        
        self::$arguments = $arguments;
    }
    
    private static function get_version_number()
    {
        $versions = array('0.9');
        
        // read the version folders inside htdocs folder
        if (is_dir(self::$htdocs_path) && !isset(self::$arguments['v']))
        {
            if ($dh = opendir(self::$htdocs_path)) 
            {
                while (($file = readdir($dh)) !== false) 
                {   
                    if (is_dir(self::$htdocs_path . "/$file") && is_numeric($file))
                    {
                        $versions[] = $file;
                    }
                }
                
                closedir($dh);
            }
        }        
        
        // get the new version number and format it to have at least 1 decimal place
        $version_number = number_format(max($versions) + 0.1, 1);
        
        return $version_number;
    }
    
    public static function set_variables()
    {
        // Use the build path argument if specified
        $build_path = isset(self::$arguments['b']) ? rtrim(self::$arguments['b'], '/') : realpath(dirname( __FILE__) . '/../../../');
        $source_path = isset(self::$arguments['s']) ? realpath(self::$arguments['s']) : realpath(dirname( __FILE__) . '/../../../');

        $java_path = isset(self::$arguments['j']) ? realpath(self::$arguments['j']) : 'java';    
        

        // create build path if it doesn't exist
        if (!is_dir($build_path))
        {
            echo "build path doesn't exist, creating build path: $build_path" . PHP_EOL;

            if (!mkdir($build_path))
            {
                // if build path was not created
                exit("ERROR: Unable to create build folder: $build_path");
            }
        }
        
        self::$htdocs_path = realpath($source_path. '/htdocs');
        
        // get the package version if specified, autogenerate otherwise        
        $package_version = isset(self::$arguments['v']) ? self::$arguments['v'] : self::get_version_number();
        
        // set the config variables
        $source_application_path = realpath($source_path .'/application');

        
        echo 'source_path: ' . $source_path;
        echo PHP_EOL;
        echo 'java path: ' . $java_path;
        echo PHP_EOL;
        echo 'build path: ' . $build_path;
        echo PHP_EOL;        
        echo "version: $package_version\n";
        
        self::$cdn_url = CDN_BASE_URL;
        self::$cdn_compressed_url = self::$cdn_compressed_url . 'resources/compressed';
        self::$package_version = $package_version;
        self::$build_path = realpath($build_path);
        self::$source_path = realpath($source_path);
        self::$templates_path = $source_application_path . '/templates';
        self::$optimized_templates_path = $source_application_path . '/optimized_templates';

        // binaries
        self::$closure_compiler  = realpath(CLOSURE_COMPILER);
        self::$yui_compressor    = realpath(YUI_COMPRESSOR);
        self::$html_compressor   = realpath(HTML_COMPRESSOR);    
        self::$version_rsync     = realpath(VERSION_RSYNC);  
        self::$build_rsync     = realpath(BUILD_RSYNC);
        
        self::$versioned_htdocs_path = self::$htdocs_path . "/$package_version";        
        self::$resources_path = realpath($source_path . '/htdocs/resources');
        self::$compressed_resources_path = self::$versioned_htdocs_path . '/resources/compressed';
        self::$scripts_path = realpath($source_path . '/build/scripts');
        
        // if --use-cdn is specified in the command line input
        self::$compressed_url_string = (true === self::$options['use-cdn']) ? self::$cdn_url . "$package_version/resources/compressed" : "/$package_version/resources/compressed";
        
        self::$java = $java_path;
        self::$css_theme_path = self::$resources_path . '/css/' . CSS_THEME;
        self::$config_path = $source_application_path . '/config';
        self::$build_config_file = $source_application_path . '/config/build.php';
        self::$css_theme_url = '/resources/css/' . CSS_THEME;
        
        self::$cc_compilation_level = (true === self::$options['advanced-optimizations']) ? 'ADVANCED_OPTIMIZATIONS' : 'SIMPLE_OPTIMIZATIONS';
        self::$hc_compilation_level = (true === self::$options['advanced-optimizations']) ? 'advanced' : 'SIMPLE_OPTIMIZATIONS';
    }
    
    public static function modify_build_config()
    {
        $backup_file = self::$config_path . '/build.bak.php';
        if (copy(self::$build_config_file, $backup_file))
        {
            echo "successfully created build config backup: $backup_file\n";
        }
        else
        {
            exit("unable to build config backup: $backup_file\n");
        }

        echo "options: \n";
        var_dump(self::$options);
        
        $config_items = array(
            'package_version' => "'" . self::$package_version . "'",
            'theme_folder'=> "'" . CSS_THEME . "'",
            'use_cdn' => (true === self::$options['use-cdn']) ? 'true' : 'false',
            'cdn_url' => "'" . self::$cdn_url . "'",
            'cdn_compressed_url' => '$config[\'cdn_url\'] . $config[\'package_version\'] . \'/resources/compressed\'',
            'compressed_url' => '$obj->config->item(\'corporate_base_url\') . $config[\'package_version\'] . \'/resources/compressed\'',
            'use_optimized_templates' => 'true'
        );
        
        $config_contents = "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');\n\n";
        $config_contents .= "\$obj =& get_instance();\n\n";
        
        foreach($config_items as $key => $value)
        {       
            $config_contents .= "\$config['$key'] = $value;\n";
        }
        
        $config_contents .= "\n";
        $config_contents .= "/* End of file build.php */\n";
        $config_contents .= "/* Location: ./application/config/build.php */";
        
        // write the config file
        file_put_contents(self::$build_config_file, $config_contents);
    }    
}

?>