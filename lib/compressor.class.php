<?php
/**
 * Abstract Compressor class
 * this class is dependent on the AWS SDK
 * 
 */
abstract class Compressor
{
    // CompressorType
    protected $_compressor_script;    
    protected $_file_type; 
    
    
    /**
     * This method will call the appropriate compress method
     * 
     * @param type $input
     * @param type $output 
     */
    public function perform_compression($input)
    {
        if(isset($input))
        {
            return $this->_compressor_script->compress($input);    
        }
        else 
        {
            echo "nothing to compress\n";
        }
        
    }
    
    /**
     * Generate hashed filename with with its "date modified" string
     * 
     * @param type $input
     * @param type $file_type
     * @param type $prefix
     * @return type 
     */
    public static function generate_filename($input, $file_type, $prefix = 'x-')
    {
        echo __METHOD__;
        echo PHP_EOL . 'input: ' . $input . PHP_EOL;
        
        $last_changed = date("F d Y H:i:s.", filemtime($input));
        
        // use the relative file path instead to easily test it.
        $relative_file_path = str_replace(Config::$resources_path, '', $input);        
        
        return $prefix . md5($relative_file_path . $last_changed) . '.' . $file_type;        
    }
    
    public static function sync_to_build()
    {        
        $source_path = Config::$source_path;
        $build_path = Config::$build_path;
        
        echo PHP_EOL . 'Synching to the target build path.' . PHP_EOL;
        // sync the required build folders, exclude folders that are not needed
        // $cmd = "sh $source_path/build_rsync.sh $source_path/. $build_path";
        $cmd = sprintf('sh %s %s/. %s', Config::$build_rsync, $source_path, $build_path);
        echo $cmd . PHP_EOL;
        $shell_output = shell_exec($cmd);
        
        echo 'shell s' . PHP_EOL;
        var_dump($shell_output);
    }
}

/**
 * JSCompressor
 */
class JSCompressor extends Compressor
{
    public function __construct()
    {
        $this->_compressor_script = new ClosureCompiler();
        
        // set the file type
        $this->_file_type = JS_FILETYPE; 
    }
}

/**
 * CSSCompressor
 */
class CSSCompressor extends Compressor
{
    public function __construct() 
    {
        $this->_compressor_script = new YUICompressor();
        $this->_file_type = CSS_FILETYPE;
    }
}

/**
 * HTMLCompressor
 */

class TemplateCompressor extends Compressor
{
    public function __construct() 
    {
        $this->_compressor_script = new HTMLCompressor();
        $this->_file_type = HTML_FILETYPE;
    }
}

/**
 * PNGCompressor
 * 
 */

class PNGCompressor extends Compressor
{
    public function __construct() 
    {
        $this->_compressor_script = new PNGOut();
        //$this->_file_type;
    }
}

/**
 * GZip Compressor
 * 
 */

class GZipCompressor extends Compressor
{
    public function __construct() 
    {
        $this->_compressor_script = new GZip();
    }
}

/**
 * CompressorScript interface makes sure that our compression classes are uniform 
 * and easier to maintain
 * 
 */
interface CompressorScript
{
    public function compress($input);
    public function prepare_input($input);
    public function prepare_output($input, $ext='');
    //public function generate_
}


class ClosureCompiler implements CompressorScript
{    
    /**
     * Compress using Google Closure Compiler.
     * 
     * @param type $input
     * @param type $output 
     */
    public function compress($input)
    {
        $output = $this->prepare_output($input);        
        $input = $this->prepare_input($input);
        
        $compressor = Config::$closure_compiler;
        $output_full_path = Config::$compressed_resources_path . '/' . JS_FILETYPE . '/' . $output;        
        
        if(file_exists($output_full_path))
        {
            echo PHP_EOL . "Skipping $output_full_path. This file was already compressed." . PHP_EOL;
            
            // do nothing if file already exists
            return $output;
        }
        
        
        $cmd = Config::$java . " -jar $compressor $input --js_output_file $output_full_path";
        $cmd = escapeshellcmd($cmd);
        
        
        echo 'executing: ' . $cmd . PHP_EOL;        
        exec($cmd, $cmd_output);
        
        // TODO: checker to verify whether the compression was successful
        
        
        return $output;

    }
    
    /**
     * Use this to pre-format/prepare the input parameters for Google Closure Compiler.
     * Different compression methods require different input params
     * 
     * 
     * @param type $input
     * @return type 
     */
    public function prepare_input($input)
    {
        echo __METHOD__ . PHP_EOL;
        
        var_dump($input);
        
        echo PHP_EOL;
        
        foreach ($input as $i) 
        {            
            $res[] = '--js ' . $i;
        }
        
        $js_input = implode(' ', $res);
        
        return $js_input;

    }
    
    public function prepare_output($input, $ext=JS_FILETYPE)
    {
        $combined_str = '';
        
        foreach ($input as $file) 
        {
            $combined_str .= Compressor::generate_filename($file, JS_FILETYPE, 'm-');
        }        
        
        return 'm-' . md5($combined_str) . '.js';
        
    }
}

class YUICompressor implements CompressorScript
{    
    /**
     * Compress using YUICompressor
     * 
     * @param type $input
     * @param type $output 
     */
    public function compress($input)
    {        
        $output = $this->prepare_output($input);
        
        $input = $this->prepare_input($input);
        
        $compressor = Config::$yui_compressor;
        $output_full_path = Config::$compressed_resources_path . '/' . CSS_FILETYPE . '/' . $output;
        
        if(file_exists($output_full_path))
        {
            echo PHP_EOL . "Skipping $output_full_path. This file was already compressed." . PHP_EOL;
            
            // do nothing if file already exists
            return $output;
        }        

        $cmd = Config::$java . " -jar $compressor $input --type css > $output_full_path";
        //$cmd = escapeshellcmd($cmd);
        echo 'executing: ' . $cmd . PHP_EOL;        

        exec($cmd, $cmd_output);            
        
        // TODO: checker to verify whether the compression was successful
        
        // when we're done, delete the temporary css file in /tmp folder        
        if (unlink($input))
        {
            return $output;
        }

    }
    
    /**
     * Use this to pre-format/prepare the input parameters for Google Closure Compiler.
     * Different compression methods require different input params
     * 
     * 
     * @param type $input
     * @return type 
     */
    public function prepare_input($input)
    {
        $combined_contents = '';
        
        foreach ($input as $file)
        {
            $combined_contents .= file_get_contents($file);
        }
        
        $temp_file = TMP . '/css-' . md5($combined_contents) . '.css';
        
        if (file_put_contents($temp_file, $combined_contents))
        {
            return $temp_file;
            
        }
        
        return false;

    }
    
    public function prepare_output($input, $ext=CSS_FILETYPE)
    {
        $combined_str = '';
        
        foreach ($input as $file) 
        {
            $combined_str .= Compressor::generate_filename($file, CSS_FILETYPE, 'm-');
        }        
        
        return 'm-' . md5($combined_str) . '.' . CSS_FILETYPE;
        
    }

}

class HTMLCompressor implements CompressorScript
{
    
    /**
     * Compress using htmlcompressor
     * http://code.google.com/p/htmlcompressor/wiki/Documentation
     * 
     * @param type $input
     * @return type $output 
     */
    public function compress($input)
    {
        // do nothing if input file doesn't exist
        if (!file_exists($input))
        {
            return false;
        }
        
        $output = $this->prepare_output($input);
        
        $input = $this->prepare_input($input);
        
        $compressor = Config::$html_compressor;
        
        $cmd = Config::$java . " -jar $compressor -o $output --type html --compress-js --compress-css --remove-intertag-spaces $input";
        
        //$cmd = escapeshellcmd($cmd);
        echo 'executing: ' . $cmd . PHP_EOL; 

        exec($cmd, $cmd_output);            
        
        return $output;
    }
    
    /**
     * Use this to pre-format/prepare the input parameters for Google Closure Compiler.
     * Different compression methods require different input params
     * 
     * 
     * @param type $input
     * @return type 
     */
    public function prepare_input($input)
    {
        return $input;

    }
    
    public function prepare_output($input, $ext='')
    {
        // do nothing here..
        return $input;
    }     
    
}

class PNGOut implements CompressorScript
{
    /**
     * Disable compression for now to save time.
     * 
     * @param type $input
     * @return string 
     */
    public function compress($input)
    {
        if (!file_exists($input))
        {
            return false;            
        }
        
        // get extension
        $ext = pathinfo($input, PATHINFO_EXTENSION);
                
        $output = $this->prepare_output($input, $ext);
        
        $input = $this->prepare_input($input);
        
        $output_full_path = Config::$compressed_resources_path . '/' . IMG_FILE . '/' . $output;                
        
        $compressed_image = IMG_FILE . '/' . $output;
        
        if (file_exists($output_full_path))
        {
            echo PHP_EOL . "Skipping $output_full_path. This file was not changed." . PHP_EOL;
            
            // do nothing if file already exists
            return $compressed_image;
        }
        
        // get the operating system and machine
        $os = php_uname('s');
        $machine = php_uname('m');
        
        // skip compression for now until we find a faster and better png compression tool
        
        /*
        // set the pngout script path based on its environment
        $pngout_subpath = ('Darwin' === $os) ? $os : "$os/$machine";
        
        $pngout = SCRIPTS_PATH . '/' . $pngout_subpath . '/' . Compressor::PNG_COMPRESSOR;
            
        echo PHP_EOL . 'Compressing: ' . $input . PHP_EOL;                
        perform compression using pngout
        exec($pngout . ' ' . $input . ' ' . $output_full_path . ' -y');         
        */

        // just copy the entire file to the output folder
        if (copy($input, $output_full_path))
        {
            return $compressed_image;
        }        
        else 
        {
            return false;
        }
        
    }
    
    /**
     * Use this to pre-format/prepare the input parameters for Google Closure Compiler.
     * Different compression methods require different input params
     * 
     * @param type $input
     * @return type 
     */
    public function prepare_input($input)
    {
        return $input;

    }        
    
    public function prepare_output($input, $ext=PNG_FILETYPE)
    {
        return Compressor::generate_filename($input, $ext, 'm-');
    }     
}

class GZip implements CompressorScript
{
    /**
     * Disable compression for now to save time.
     * 
     * @param type $input
     * @return string 
     */
    public function compress($input)
    {
        // gzip compress each file with the .gz extension on each of them
        foreach ($input as $file_to_compress)
        {   
            $cmd = GZIP . " -c $file_to_compress > $file_to_compress.gz";
            echo "running: $cmd\n";
            $shell_out = shell_exec($cmd);
            echo 'gzip result: ';
            var_dump($shell_out);
        }
        
        // remove the .gz extension and overwrite the existing files, after making sure
        // that the files were successfully gzipped
        
        foreach ($input as $compressed_file)
        {
            if (true === copy($compressed_file . ".gz", $compressed_file))
            {
                // delete the gz file if successfully copied
                if (unlink($compressed_file . ".gz"))
                {
                    echo "successfully deleted $compressed_file.gz file";
                }
                
            }
            else
            {
                echo "something went wrong while copying the file and overwriting the original: $compressed_file";
            }
        }
    }
    
    /**     
     * Different compression methods require different input params
     * Use this to pre-format/prepare the input parameters for Google Closure Compiler.
     * 
     * @param type $input
     * @return type 
     */
    public function prepare_input($input)
    {
        return $input;

    }        
    
    public function prepare_output($input, $ext=PNG_FILETYPE)
    {
        return Compressor::generate_filename($input, $ext, 'm-');
    }     
}