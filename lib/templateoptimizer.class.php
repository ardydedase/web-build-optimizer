<?php

/**
 *
 */
class TemplateOptimizer
{
    // Regex declarations, where all the magic happens.    
    private static $_pattern_js_block           = "/<!--start_js-->(.*?)<!--end_js-->/ms";
    private static $_pattern_js_files           = "/<script[^>]+src=[\"']([^\"']+\.js)[\"']/";
    private static $_pattern_css_block          = "/<!--start_css-->(.*?)<!--end_css-->/ms";
    private static $_pattern_css_files          = "/<link[^>]+href=[\"']([^\"']+\.css)[\"']/";
    private static $_pattern_img_path           = "/<img[^>]+src=\"(.+\.(png|jpe?g|gif))\"/";    
    private static $_pattern_css_img_path       = "/url\([\"']?(.+\.(png|jpe?g|gif))[\"']?\)/";
    
    public static $_pattern_templates_path      = "/\/templates\/(.+)/i";    
    public static $_pattern_htdocs_path         = "/\/htdocs\/(.+)/i";        
    
    
    // Replace patterns
    private static $_js_html                    = '<script src="{src}" type="text/javascript"></script>';
    private static $_css_html                   = '<link href="{src}" rel="stylesheet"/>';    
    
    /**
     * Optimize images to be used, Compress png and upload them to Amazon S3
     * 
     * @param type $block
     * @param type $compressor
     * @param type $pattern
     * @return type 
     */
    private static function _optimize_images($block, $compressor)
    {        
        // Optimize all the images in the CSS file.
        
        // get matching block of script/stylesheet file references
        $res_block = preg_match(self::$_pattern_css_block, $block, $matches_block);
        
        if (empty($matches_block))
        {
            // do nothing and return the original block
            return $block;
        } 
        
        // get matching files for script/stylesheet file references        
        $res_files = preg_match_all(self::$_pattern_css_files, $matches_block[1], $matches_files);
        
        if (empty($matches_files))
        {
            // do nothing and return the original block
            return $block;
        }
        
        echo 'output: ' . PHP_EOL;
        
        $css_files = self::fix_path($matches_files[1]);
        
        foreach($css_files as $css_file)
        {
            $css_content = file_get_contents($css_file);
            $res_css = preg_match_all(self::$_pattern_css_img_path, $css_content, $matches_css_img);
            
            if(empty($matches_css_img[1]))
            {
                continue;
            }
            
            $css_images = array_filter($matches_css_img[1]);            

            $css_images_full_path = self::fix_path($css_images);
            
            foreach($css_images_full_path as $idx=>$css_image)
            {
                echo PHP_EOL . 'css image: ' . $css_image . PHP_EOL;

                $compressed_css_image_path = Config::$compressed_url_string . '/' . $compressor->perform_compression($css_image);

                echo PHP_EOL . 'compressed css image: ' . $compressed_css_image_path . PHP_EOL;

                if(!is_null($compressed_css_image_path)) 
                {
                    $css_image_map[$compressed_css_image_path] = $css_images[$idx];
                }
                
            }
            
            if(!empty($css_image_map)) 
            {
                var_dump($css_image_map);            
                $css_content = str_replace(array_values($css_image_map), array_keys($css_image_map), $css_content);                
                
                // get the hashed filename of the css file
                // $temp_css_filename = Compressor::generate_filename($css_file, CSSCompressor::CSS_FILETYPE, CSSCompressor::TEMP_CSS_PREFIX);
                $temp_css_file = 'tmp-' . basename($css_file);
                $temp_css = Config::$css_theme_path . '/' . $temp_css_file;
                
                // keep this, to be deleted later
                $temp_css_files[] = $temp_css;
                
                // write the optimized image url/path changes to the css file
                if (file_put_contents($temp_css, $css_content))
                {
                    echo PHP_EOL;
                    echo 'successfully updated image paths/urls for css file: ' . $temp_css . PHP_EOL;
                }
                else 
                {
                    echo 'there was a problem writing the css file..';
                }
                
                $temp_css_url = Config::$css_theme_url . '/' . $temp_css_file;
                $css_url = Config::$css_theme_url . '/' . basename($css_file);
                
                // Create a css map array. 
                // This will be used to search and replace existing css references.
                
                $css_map[$temp_css_url] = $css_url;
            }
        }
        
        // if css image references were changed
        if (!empty($css_map))
        {
            echo 'CSS Map: ' . PHP_EOL;
            var_dump($css_map);
            
            $block = str_replace(array_values($css_map), array_keys($css_map), $block);
            
            
            echo PHP_EOL;
            echo 'HTML Block after temp CSSs str_replace: ';
            echo PHP_EOL;
            
            // compress and update css files as well after referenced images were changed/compressed
            $block = self::_optimize_script(
                        $block, 
                        new CSSCompressor(), 
                        self::$_pattern_css_block, 
                        self::$_pattern_css_files, 
                        self::$_css_html, 
                        CSS_FILETYPE
                    );
            
            if ($block)
            {
                // delete the temp files when we've successfully compressed css and updated our template file
                self::delete_files($temp_css_files);
            }
        }
        
        // Optimize all the img tag images in the html template.
        
        // extract all the img src url
        $res_img = preg_match_all(self::$_pattern_img_path, $block, $matches_img);
        
        // extract all the background image urls from inline css declarations
        $res_inline_bg = preg_match_all(self::$_pattern_css_img_path, $block, $matches_inline_bg);
        
        echo PHP_EOL;
        echo 'inline css bg matches: ' . PHP_EOL;
        var_dump($matches_inline_bg[1]);
        
        echo 'html img images: ' . PHP_EOL;
        var_dump($matches_img[1]);
        
        // merge the results and make sure that there are no duplicates
        $images = array_unique(
            array_merge(
                    $matches_inline_bg[1], $matches_img[1]
            )
        );
        
        echo 'all images: ' . PHP_EOL;
        var_dump($images);
        
        
        if (empty($matches_img[1]) && empty($matches_inline_bg[1]))
        {
            return $block;
        }
        
        // make sure that it is the complete path
        $images_full_path = self::fix_path($images);
        
        echo 'images_full_path: ' . PHP_EOL;
        var_dump($images_full_path);
        
        foreach ($images_full_path as $idx=>$image) 
        {
            echo PHP_EOL . 'image: ' . $image . PHP_EOL;

            $compressed_image = $compressor->perform_compression($image);

            echo PHP_EOL . 'compressed image: ' . $compressed_image . PHP_EOL;
            $compressed_image_path = COMPRESSED_URL . '/' . $compressed_image;

            if($compressed_image)
            {
                $image_map[$compressed_image_path] = $images[$idx];
            }

            //$compressed_images_map[] = $compressed_image_path;
            //$images_map[] = $images[$idx];

            echo PHP_EOL;
            echo 'compressed image path: ' . $compressed_image_path . PHP_EOL;
        }
        
        if (!empty($image_map))            
        {
            $block = str_replace(array_values($image_map), array_keys($image_map), $block);    
        }
        
        return $block;
    }
    
    /**
     * Optimize scripts. This includes JavaScript and CSS. Grab the html code, then find the JavaScript and CSS 
     * references. Combine and compress these files.
     * 
     * @param type $block
     * @param type $compressor
     * @param type $pattern_block
     * @param type $pattern_files
     * @param type $script_tpl
     * @param type $file_type
     * @return string $res_replaced
     */
    private static function _optimize_script($block, $compressor, $pattern_block, $pattern_files, $script_tpl, $file_type='js')
    {   
        // get matching block of script/stylesheet file references
        $res_block = preg_match($pattern_block, $block, $matches_block);
        
        if (empty($matches_block))
        {
            // do nothing and return the original block
            return $block;
        } 
        
        // get matching files for script/stylesheet file references        
        $res_files = preg_match_all($pattern_files, $matches_block[1], $matches_files);
        
        if (empty($matches_files))
        {
            // do nothing and return the original block
            return $block;
        }
        
        echo 'output: ' . PHP_EOL;
        
        $files = self::fix_path($matches_files[1]);
        
        // using the Compressor
        $compressed_file = $compressor->perform_compression($files);
        
        if (null === $compressed_file)
        {
           return $block; 
        }
        
        
        echo PHP_EOL;
        echo "compressed $file_type: " . $compressed_file;
        
        $script_path = COMPRESSED_URL . "/$file_type/" . $compressed_file;
        
        $compressed_script_block = str_replace('{src}', $script_path, $script_tpl);
        
        echo PHP_EOL;
        echo "$file_type script type: " . $compressed_script_block . PHP_EOL;
        
        $res_replaced = preg_replace($pattern_block, $compressed_script_block, $block);        
        
        
        return $res_replaced;                
    }
    
    /**
     * Optimize HTML Template
     * 
     * @param type $template_path
     * @return string 
     */
    private static function _optimize_html($template_path)
    {
        $compressor = new TemplateCompressor();
        return $compressor->perform_compression($template_path);
    }
    
    public static function optimize_template($file_path, $options=array())
    {
        preg_match(self::$_pattern_templates_path, $file_path, $matches);    
        
        $file = $matches[1];
        
        echo PHP_EOL . 'name: ' . $file . PHP_EOL;
        
        // template file block/contents
        $block = file_get_contents($file_path);
        
        // Optimize the assets and resources one-by-one
        
        // Optimize HTML images
        
        $block = self::_optimize_images($block, new PNGCompressor());
        
        // Optimize JavaScript
        
        $block = self::_optimize_script(
                    $block, 
                    new JSCompressor(), 
                    self::$_pattern_js_block, 
                    self::$_pattern_js_files, 
                    self::$_js_html, 
                    JS_FILETYPE
                );
        
        
        // Optimize CSS
        
        $block = self::_optimize_script(
                    $block, 
                    new CSSCompressor(), 
                    self::$_pattern_css_block, 
                    self::$_pattern_css_files, 
                    self::$_css_html, 
                    CSS_FILETYPE
                );
        
        // write the optmized template file
        
        
        $template_path = Config::$optimized_templates_path . '/' . $file;
        
        $fp_return = file_put_contents($template_path, $block);
        
        echo PHP_EOL;
        echo 'fp_return: ' . $fp_return . PHP_EOL;
        
        $template_path = self::_optimize_html($template_path);
        
        
        echo PHP_EOL;
        echo 'template path is in: ' . $template_path;
        echo PHP_EOL;
        
    }
    
    /**
     * Lame delete files method
     * 
     * @param type $files   
     */
    public static function delete_files($files)
    {
        foreach ($files as $file) 
        {
            unlink($file);
        }
    }
    
    /**
     * Make sure that file paths are correct
     * 
     * @param array $files
     * @return string 
     */
    public static function fix_path($files)
    {
        foreach ($files as $file)            
        {
            $file = ltrim($file, '/');
            $fixed_file_paths[] = Config::$htdocs_path . '/' . $file;
        }
        
        return $fixed_file_paths;
    }
    
    /**
     * This is used to check if optimized folder paths exist
     * @param type $folder_path 
     */
    public static function prepare_deployment_folder($source_path, $target_path, $path_pattern)
    {
        preg_match($path_pattern, $source_path, $matches);    
        
        var_dump($matches);
        
        $folder = $matches[1];

        if(!is_dir($target_path))
        {
            mkdir($target_path);
        }
        
        $optimized_folder_path = rtrim($target_path, '/') . '/' . $folder;
        
        echo "optimized_folder_path: $optimized_folder_path\n";

        if (!is_dir($optimized_folder_path))
        {
            return mkdir($optimized_folder_path, 0777);
        }        
    }

}