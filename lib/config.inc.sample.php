<?php if (!class_exists('FileFilterIterator')) die('No direct access allowed.');
/**
 * Rename this file to config.inc.php
 */

define('AWS_ACCESS_KEY', '');
define('AWS_SECRET_KEY', '');
//define('AWS_S3_BUCKET', 'corporate.cdn.mig33.com');
define('AWS_S3_BUCKET', 'xx.xx.xx.com');

// compressed url PHPTal variable
define('COMPRESSED_URL', '${compressed_url}');

// CSS Theme name to use
define('CSS_THEME', 'v3');

define('CDN_BASE_URL', 'http://xx.xx.xx.com.s3.amazonaws.com/');

// i don't know if this is necessary
define('TMP', '/tmp');


define('JS_FILETYPE', 'js');
define('CSS_FILETYPE', 'css');
define('HTML_FILETYPE', 'html');
define('PNG_FILETYPE', 'png');
define('IMG_FILE', 'img');
define('JS_EXT_VAR', '${js_ext}');
define('CSS_EXT_VAR', '${css_ext}');

define('CLOSURE_COMPILER', 'scripts/compiler.jar');
define('YUI_COMPRESSOR', 'scripts/yuicompressor-2.4.6.jar');
define('HTML_COMPRESSOR', 'scripts/htmlcompressor-1.5.3.jar');
define('VERSION_RSYNC', 'scripts/version_rsync.sh');
define('BUILD_RSYNC', 'scripts/build_rsync.sh');
define('PNG_COMPRESSOR', 'pngout');
define('GZIP', 'gzip');

define('TEMP_CSS_PREFIX', 'corpcss-');
// end of file config.inc.php